<?php
 
/**
 * Implementation of hook_configure().
 *
 * @todo convert in t() functions with parameters
 */
 
function wss_bluga_settings() {
  $form = array();
  
  $form['wss_bluga_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('API key'),
      '#default_value' => variable_get('wss_bluga_api_key', ''),
      '#description' => t('Your Bluga API key. If you don\'t have one yet, you can get one !link.', array('!link' => l(t('here'), 'http://webthumb.bluga.net/register'))),
  );
  
  $form['wss_bluga_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => variable_get('wss_thumbalizr_width', '300'),
      '#description' => t('The size (width) of the generated image (height is automatically calculated by Thumbalizr).') .'<br/>'. t('Default: %default', array('%default' => 300)),
      
  );
  
  
  $form['wss_bluga_delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Delay'),
      '#default_value' =>  variable_get('wss_bluga_delay', ''),
      '#description' => t('Delay to wait before taking the snapshot, Ranges from 1 to 15 seconds.') .'<br/>'. t('Default: %default', array('%default' => 3)) .'<br/><b>'. t('NOT IMPLEMENTED YET!') .'</b>',
  );
  
  $form['wss_bluga_image_type'] = array(
      '#type' => 'select',
      '#title' => t('Image type'),
      '#options' => array('' => t('Default'), 'jpg' => t('JPG'), 'png' => t('PNG')),
      '#default_value' => variable_get('wss_bluga_image_type', ''),
      '#description' => t('The type of image to generate.') .'<br/>'. t('Default: %default', array('%default' => t('JPG'))) .'<br/><b>'. t('NOT IMPLEMENTED YET!') .'</b>',
  );
  
 
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  
  return $form;
}


/** implementation of hook_screenshot_settings_validate
    **/
function wss_bluga_settings_validate($form_id, &$form_state) {
  $width = $form_state['values']['wss_bluga_width'];
  
  if ($width < 300 || $width > 1024 ){
      form_set_error('website_screenshot_width', t('Width must be an integer between %min_width and %max_width.', array('%min_width' => $min, '%max_width' => $max)));
  }

}


/** implementation of hook_screenshot_settings_submit
    **/
function wss_bluga_settings_submit($form_id, &$form_state) {
  
}