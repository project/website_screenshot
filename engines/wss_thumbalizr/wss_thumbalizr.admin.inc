<?php
 
/**
 * Implementation of hook_configure().
 *
 * @todo convert in t() functions with parameters
 */
 
function wss_thumbalizr_settings() {
  $form = array();
  
  $form['wss_thumbalizr_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('API key'),
      '#default_value' => variable_get('wss_thumbalizr_api_key', ''),
      '#description' => t('Your Thumbalizr API key. If you don\'t have one yet, you can get one !link.', array('!link' => l(t('here'), 'http://www.thumbalizr.com/member/signup.php'))),
  );
  
  
  $form['wss_thumbalizr_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => variable_get('wss_thumbalizr_width', '300'),
      '#description' => t('The size (width) of the generated image (height is automatically calculated by Thumbalizr).') .'<br/>'. t('Default: %default', array('%default' => 300)),
      
  );
  
  
  $form['screenshot_thumbalizr_quality'] = array(
    '#type' => 'textfield',
      '#title' => t('Image quality'),
      '#required' => FALSE,
      '#default_value' => variable_get('screenshot_thumbalizr_quality', ''),
      '#description' => t('The quality of the generated image.') .'<br/>'. t('Default: %default', array('%default' => 90)) .'<br /><b>NOT IMPLEMENTED YET!</b>',
  );
  
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save settings'),
  );
  
  return $form;
}


/** implementation of hook_screenshot_settings_validate
    **/
function wss_thumbalizr_settings_validate($form_id, &$form_state) {
  $width = $form_state['values']['wss_thumbalizr_width'];
  $key = $form_state['values']['wss_thumbalizr_api_key'];
  if (empty($key)) {
    $min = 100;
    $max = 300;  
  }
  else{
    $min = 100;
    $max = 1280;  
  }

  if ($width < $min || $width > $max ){
      form_set_error('website_screenshot_width', t('Width must be an integer between %min_width and %max_width.', array('%min_width' => $min, '%max_width' => $max)));
  }
}


/** implementation of hook_screenshot_settings_submit
    **/
function wss_thumbalizr_settings_submit($form_id, &$form_state) {
  
}