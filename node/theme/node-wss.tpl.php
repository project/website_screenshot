<?php
?>

<div class="website_screenshot_content">
  <?php if ($wss_node_image): ?>
  <div class="website_screenshot_image">
    <?php print $wss_node_image; ?>
  </div>
  <?php endif; ?>
  <div class="wss_node_link">
    <?php print $wss_node_link; ?>
  </div>
</div>