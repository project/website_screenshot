<?php

/**
 * @file
 * Views integration for Website Screenshot module.
 */


/**
 * Implementation of hook_views_data()
 */
function website_screenshot_views_data() {

  $data['wss']['table']['group'] = t('Website Screenshot');
  
  //Commented Because we're no need of a views base type
  /*$data['website_screenshot']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Website Screenshot'),
    'help' => t("Website Screenshot views integration"),
    'weight' => -10,
  );*/

  $data['wss']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['wss']['nid'] = array(
     'title' => t('Website Screenshot content'),
     'help' => t('Website Screenshot node.'),
     'relationship' => array(
       'base' => 'node',
       'field' => 'nid',
       'handler' => 'views_handler_relationship',
       'label' => t('Website Screenshot node'),
     ),
   );

  $data['wss']['url'] = array(
     'title' => t('URL field'),
     'help' => t('The site URL field.'),
     'field' => array(
       'handler' => 'views_handler_field_url',
       'click sortable' => TRUE,
     ),
     'sort' => array(
       'handler' => 'views_handler_sort',
     ),
     'filter' => array(
       'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
   );

  $data['wss']['request_time'] = array(
     'title' => t('Screeshot Request Time'),
     'help' => t('Request Timestamp field.'),
     'field' => array(
       'handler' => 'views_handler_field_date',
       'click sortable' => TRUE,
     ),
     'sort' => array(
       'handler' => 'views_handler_sort_date',
     ),
     'filter' => array(
       'handler' => 'views_handler_filter_date',
     ),
   );
  
  $data['wss']['file'] = array(
    'title' => t('Screeshot image'),
    'help' => t('Screeshot image'),
    'field' => array(
      'handler' => 'website_screenshot_handler_field_file',
      'click sortable' => FALSE,
    ),
    
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function website_screenshot_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'website_screenshot') .'/views',
    ),
    'handlers' => array(
      'website_screenshot_handler_field_file' => array(
        'parent' => 'views_handler_field_node',
      ),
    ),
  );
}

/**
 * A custom theme function.
 *
 * By using this function to format our node-specific information, themes
 * can override this presentation if they wish. We also wrap the default
 * presentation in a CSS class that is prefixed by the module name. This
 * way, style sheets can modify the output without requiring theme code.
 */

function theme_website_screenshot_image_display($node, $imagecache_preset) {
  //drupal_set_message("<pre>". print_r($attributes,true)."</pre>");
  $output .= '<div class="website_screenshot_content">'."\n";
  if (!$node->file) {
    $output .= '<div class="website_screenshot_link">'."\n";
    $output .= l($node->url, $node->url);
    $output .= '</div>';
  }
  else{
    $output .= '<div class="website_screenshot_image">'."\n";
    $attributes['rel'] = "lightbox";
    $filename = file_directory_path() .'/'. variable_get('website_screenshot_basepath', 'website_screenshot') .'/'. $node->file;
    $output .= "<a href='$filename' ". drupal_attributes($attributes) .'>'
            . theme('imagecache', $imagecache_preset, $filename, 'screen shot of '. check_plain($node->url) , check_plain($node->url),  $attributes2)
            .'</a>'; 
    $output .= '</div>';
   
  }
  $output .= '</div>';
  return $output;
}

