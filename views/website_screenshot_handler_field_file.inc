<?php

/**
 * @file
 * This include file implements views functionality on behalf of the
 * website_screenshot.module.
 */

/**
 * Field handler to provide simple renderer that turns screenshoot.
 *
 */
class website_screenshot_handler_field_file extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();

    $options['display_as_link'] = array('default' => FALSE);
    $options['display_as_screenshot'] = array('default' => TRUE);
    return $options;
  }

  /**
   * Provide link to the page being visited.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['display_as_screenshot'] = array(
      '#title' => t('Display as Screenshot Image'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_as_screenshot']),
    );
    
    $imagecache_presets = imagecache_presets();
    foreach($imagecache_presets as $imagecache_preset){
      $presets[$imagecache_preset['presetname']] = $imagecache_preset['presetname'];
    }
    $form['wss_imagecache_preset'] = array(
      '#type' => 'select',
      '#title' => t('Imagecache Preset'),
      '#required' => TRUE,
      '#options' => $presets, 
      '#default_value' => $this->options['wss_imagecache_preset'],
      '#description' => t('Select your imagecache profile.')
    );
  
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    if (!empty($this->options['display_as_screenshot'])) {
      $node = node_load($values->nid);
      return theme('website_screenshot_image_display', $node, $this->options['wss_imagecache_preset']);
    }
    else {
      return $value;
    }
  }
}