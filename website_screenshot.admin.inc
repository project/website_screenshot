<?php
 
/**
 * Implementation of hook_configure().
 *
 * @todo convert in t() functions with parameters
 */

function website_screenshot_configure() {
  $lightbox_ispresent = module_exists('lightbox2') || module_exists('jLightbox');
  if ($lightbox_ispresent) {
    drupal_set_message('Lightbox is installed with '. l('Lightbox2', 'http://drupal.org/project/lightbox2') .' or '. l('jQuery Lightbox', 'http://drupal.org/project/jlightbox') .' modules. You can use this functionality.', 'status', FALSE);
  }
  else {drupal_set_message('Lightbox is not present.<br />To use javascript zoom feature you should download and install '. l('Lightbox2', 'http://drupal.org/project/lightbox2') .' or '. l('jQuery Lightbox', 'http://drupal.org/project/jlightbox') .' module.', 'warning');}

  

  $form = array();
  $form['website_screenshot'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration'),
    '#weight' => -10,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    );

  $form['website_screenshot']['website_screenshot_engine'] = array(
    '#type' => 'select',
    '#title' => t('Screenshot Engine'),
    '#required' => TRUE,
    '#options' => array('bluga' => 'bluga', 'thumbalizr' => 'thumbalizr'), 
    '#default_value' => variable_get('website_screenshot_engine', 'thumbalizr'),
    '#description' => t('Select your Screenshot service provider.')
    );
  
  
  
  $form['#validate'][] = 'website_screenshot_configure_validate';
  return system_settings_form($form);
}

function website_screenshot_configure_validate($form_id, &$form_state) {
  
  
  
}

